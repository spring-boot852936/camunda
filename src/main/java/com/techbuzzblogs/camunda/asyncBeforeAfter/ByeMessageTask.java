package com.techbuzzblogs.camunda.asyncBeforeAfter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("ByeMessageTask")
public class ByeMessageTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Bye Message delegateExecution = " + delegateExecution);
    }
}