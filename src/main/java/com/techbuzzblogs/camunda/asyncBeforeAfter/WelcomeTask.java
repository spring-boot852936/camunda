package com.techbuzzblogs.camunda.asyncBeforeAfter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("WelcomeTask")
public class WelcomeTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Welcome delegateExecution = " + delegateExecution);
    }
}
