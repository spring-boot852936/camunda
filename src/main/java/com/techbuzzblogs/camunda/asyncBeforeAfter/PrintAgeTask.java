package com.techbuzzblogs.camunda.asyncBeforeAfter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("PrintAgeTask")
public class PrintAgeTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Print Age delegateExecution = " + delegateExecution);
        System.out.println("Age = " + delegateExecution.getVariable("age").toString());
        System.out.println("Age = " + delegateExecution.getVariable("suername").toString());
    }
}