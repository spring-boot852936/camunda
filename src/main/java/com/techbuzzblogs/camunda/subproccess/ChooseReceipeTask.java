package com.techbuzzblogs.camunda.subproccess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("ChooseReceipeTask")
public class ChooseReceipeTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Choose Receipe delegateExecution = " + delegateExecution);
    }
}