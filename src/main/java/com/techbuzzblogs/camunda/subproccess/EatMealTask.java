package com.techbuzzblogs.camunda.subproccess;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("EatMealTask")

public class EatMealTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("Eat Meal delegateExecution = " + delegateExecution);
    }
}
