package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.model.bpmn.instance.ServiceTask;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperties;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaProperty;
import org.springframework.stereotype.Service;

@Service("WelcomeTasks")
public class WelcomeTasks implements JavaDelegate {
    private Expression qaUrl;
    private Expression devUrl;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("welcometasks delegateExecution = " + delegateExecution);
        System.out.println("injection filds= " + qaUrl.getValue(delegateExecution));
        System.out.println("extension properties qaUrl= " + qaUrl.getValue(delegateExecution));
        System.out.println("extension properties devUrl= " + devUrl.getValue(delegateExecution));

        ServiceTask serviceTask = (ServiceTask) delegateExecution.getBpmnModelElementInstance();
        CamundaProperties camundaProperties = serviceTask.getExtensionElements().getElementsQuery().filterByType(CamundaProperties.class).singleResult();
        for (CamundaProperty camundaProperty : camundaProperties.getCamundaProperties()) {
            System.out.println("name property=> " + camundaProperty.getCamundaName());
            System.out.println("value property=> " + camundaProperty.getCamundaValue());
        }
    }
}
