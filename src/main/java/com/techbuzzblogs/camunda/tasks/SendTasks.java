package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("SendTasks")

public class SendTasks implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("sendtasks delegateExecution = " + delegateExecution);
        System.out.println("My local variable value is  = " + delegateExecution.getVariable("local-gender"));
    }
}
