package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("AdultOrChildPrintTasks")
public class AdultOrChildPrintTasks implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("AdultOrChildPrintTasks = " + delegateExecution.getVariable("child-or-adult"));
    }
}
