package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("FinalCheck")
public class FinalCheck implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("FinalCheck delegateExecution = " + delegateExecution.getVariable("global-gender"));
    }
}
