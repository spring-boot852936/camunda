package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("ServiceAgePrint")
public class ServiceAgePrint implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("output is  = " + delegateExecution.getVariable("output"));
        System.out.println("**********************************");
        System.out.println("getVariables  = " + delegateExecution.getVariables());

    }
}
