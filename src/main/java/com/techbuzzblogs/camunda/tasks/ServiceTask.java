package com.techbuzzblogs.camunda.tasks;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("ServiceTask")
public class ServiceTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("service task delegateExecution = " + delegateExecution);
        System.out.println("parent userName = " + delegateExecution.getVariable("userName"));
    }
}
