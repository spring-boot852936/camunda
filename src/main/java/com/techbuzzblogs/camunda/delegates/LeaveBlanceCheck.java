package com.techbuzzblogs.camunda.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import org.slf4j.Logger;

@Service("LeaveBlanceCheck")
public class LeaveBlanceCheck implements JavaDelegate {

    public static final int LEAVE_BALANCE =5;
    private final Logger logger =  LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        if (LEAVE_BALANCE > 0){
            logger.info("Employee have Leave Balance!!!");
        }else{
            logger.error("Employee don't have Leave Balance!!!");
        }
    }
}