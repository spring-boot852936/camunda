package com.techbuzzblogs.camunda.listeners;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Service;

@Service("SampleExecutionListener")
public class SampleExecutionListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        System.out.println("SampleExecutionListener => delegateExecution = " + delegateExecution);
    }
}
