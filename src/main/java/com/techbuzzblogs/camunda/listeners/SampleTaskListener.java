package com.techbuzzblogs.camunda.listeners;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.springframework.stereotype.Service;

@Service("SampleTaskListener")
public class SampleTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        System.out.println("SampleTaskListener => delegateTask = " + delegateTask);
    }
}
